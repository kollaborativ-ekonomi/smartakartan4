# Smarta Kartan 4.0

This repository is discontinued. Please go to https://gitlab.com/kollaborativ-ekonomi-sverige/smartakartan.

The code for the current version 3 (at the time of writing) is available here:
https://github.com/GoteborgsStad/smartakartan3.0
